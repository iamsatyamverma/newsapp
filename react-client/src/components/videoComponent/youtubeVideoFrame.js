import React, { Component } from 'react';

class YoutubeVideoFrame extends Component {
  render() {
    return (
        <div className="Video-frame">
         <div><iframe width="480" height="270" src={this.props.videoLink} frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe></div>
      </div>
    )
  }
}

export default YoutubeVideoFrame;