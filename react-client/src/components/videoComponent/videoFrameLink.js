import React, { Component } from 'react';

class VideoFrameLink extends Component {
  render() {
    return (
        <a className="Video-tile" href={this.props.redirectTo}>
            <div className="Video-tile__image"><img src={this.props.imageLink} alt={this.props.title} /></div>
            <div className="Video-tile__content">
                <div className="Header Header--tiny"></div>
                <h3 className="Header Header--medium">{this.props.title}</h3>
            </div>
        </a>
    )
  }
}

export default VideoFrameLink;