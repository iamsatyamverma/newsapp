import React, { Component } from 'react';
import YoutubeVideoFrame from './youtubeVideoFrame';

class VideoTileHero extends Component {
  render() {
    return (
    <div className="Video-tile Video-hero row">
        <div className="col-md-6">
            {/* <div className="Video-frame">
                <div><iframe width="480" height="270" src="//www.youtube.com/embed/P-9iOwxHDAU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe></div>
            </div> */}
            <YoutubeVideoFrame videoLink={this.props.videoLink}/>
        </div>

        <div className="col-md-6">
            <div className="Video-hero__inner">
                <h2 className="Header Header--large">{this.props.videoHeader}</h2>
                <p>{this.props.videoText}</p>
            </div>
        </div>
    </div>
    )
  }
}

export default VideoTileHero;