import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';  
import {intlShape, injectIntl, defineMessages} from 'react-intl';
import {updateSubscription} from '../../actions/index';
import SocialLinks from '../commonComponent/socialLink';

class Footer extends Component {
  
  subscribeNow (event) {
    event.preventDefault()
    event.stopPropagation();
    event.nativeEvent.stopImmediatePropagation();
    this.props.dispatch(updateSubscription());
  }

  createSubscribeButton(current_pitchbooks){
    if (current_pitchbooks.length) {
    return <div className="col-md-2 d-none d-sm-block">
    <div className="Divider"></div>
    <h4 className="Header Header--tiny font-size-12 blue-text">Pitchbooks</h4>
    <img src={current_pitchbooks[0]['image']['guid']} alt="Afarinsighrs Pitchbook Issue" />
    <a className="font-size-12" href="" onClick={(event)=>this.subscribeNow(event)} className="Button--red-inverted Button--block">Subscribe</a>
    </div>
    }
    return ''
  }

  render() {
    const {formatMessage} = this.props.intl;

    return (
      <div>
    <footer className="App-footer">
   <div className="App-footer__inner container">
      <div className="row">
         <div className="col-md-10 mb-3 mb-md-0" style={{'float':'none', 'margin':'0 auto'}}>
            <div className="row">

  <div className="col-md-4 mb-3 mb-md-0">
      <div className="Divider Divider--long Divider--thick"></div>
      <img src="/static/media/logo-blue.svg" className="App-logo" alt="logo" />
      <SocialLinks />

      <address>
      InProved Pte Ltd.<br/>(ACRA Reg No. 201602269C)<br/>80 Raffles Place, UOB Plaza 2,<br /> 11-02, Singapore 048624
      </address>
      <dl>
         <div>
            <dt className="Header Header--tiny">Telephone</dt>
            <dd>65 9 745 5372</dd>
         </div>
         <div>
            <dt className="Header Header--tiny">Email</dt>
            <dd>enquiry@inproved.com</dd>
         </div>
      </dl>
   </div>

               <div className="col-md-3 mb-3 mb-md-0">
                  <div className="Divider"></div>
                  <h4 className="Header Header--tiny font-size-12 blue-text">{formatMessage({id:"footer.About"})}</h4>
                  <ul>
                     <li><a className="font-size-12" href="/about">{formatMessage({id:"footer.About"})}</a></li>
                     <li><a className="font-size-12" href="/advertising">{formatMessage({id:"footer.Advertising"})}</a></li>
                     <li><a className="font-size-12" href="/submissions">{formatMessage({id:"footer.Become_a_Pitch-book_provider"})}</a></li>
                     <li><a className="font-size-12" href="/suggestions">{formatMessage({id:"footer.Pitch-book_suggestions"})}</a></li>
                     <li className="d-none d-md-block">&nbsp;</li>
                     <li><a className="font-size-12" href="/contact">Contact Us</a></li>
                  </ul>
               </div>
               <div className="col-md-3 mb-3 mb-md-0">
                  <div className="Divider"></div>
                  <h4 className="Header Header--tiny font-size-12 blue-text">{formatMessage({id:"footer.Resources"})}</h4>
                  <ul>
                     <li><a className="font-size-12" href="/webinars">{formatMessage({id:"footer.Webinars"})}</a></li>
                     <li><a className="font-size-12" href="/events">{formatMessage({id:"footer.Events"})}</a></li>
                     <li><a className="font-size-12" href="/resources/white-papers">{formatMessage({id:"footer.White_Papers_And_Reports"})}</a></li>
                     <li><a className="font-size-12" href="/newsletters">{formatMessage({id:"footer.Newsletters"})}</a></li>
                     <li><a className="font-size-12" href="/videos">{formatMessage({id:"footer.Videos"})}</a></li>
                  </ul>
               </div>

               {this.createSubscribeButton(this.props.pitchbooks.current)}

            </div>
         </div>
      </div>
   </div>
</footer>

<div className="App-Footer__bottom">
   <div className="container">
      <div className="col-lg-12" style={{color:'#1d1d1b', 'font-style': 'italic'}}>
         <div>

            <p>Afar Insights is a Registered Trademark and the property of InProved Pte Ltd (ACRA Reg No. 201602269C)</p>
            <p>
               Copyright © 2018 InProved Pte Ltd, All Rights reserved. <a href="/privacy-policy"> Privacy Policy </a>
            </p>
         </div>
      </div>
   </div>
</div>

</div>

    )
  }
}

Footer.propTypes = {  
  pitchbooks: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {  
    return {
      pitchbooks: state.pitchbook.pitchbooks
    };
  }

export default connect(mapStateToProps)(injectIntl(Footer));
