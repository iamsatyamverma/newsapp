import React, { Component } from 'react';

class CompanyLogo extends Component {
  render() {
    return (
        <a className="App-logo-container" href={this.props.redirectTo}>
        <img src={this.props.url} className="App-logo" alt="logo"></img>
        </a>
    )
  }
}

export default CompanyLogo;