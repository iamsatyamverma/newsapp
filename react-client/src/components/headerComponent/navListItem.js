import React, { Component } from 'react';
import LocalizedLink from '../commonComponent/localizedLink';


class NavListItem extends Component {
    constructor(props) {
        super(props);

        this.showOptions = this.showOptions.bind(this);
    
        this.state = {
          is_visible: false
        }
    
      }

    showOptions(event) {
        event.preventDefault()
        event.stopPropagation();
        event.nativeEvent.stopImmediatePropagation();
        this.setState({is_visible: !this.state.is_visible})
      }

render() {    
    var options = this.props.options;
    let navListClass = this.state.is_visible ? "Accordion-list is-visible" : "Accordion-list";

    var optionsList =  options.map(function(oneOptionDict, index) {
        return <li className="Dropdown__item">
        <LocalizedLink to={oneOptionDict['redirectTo']} children={oneOptionDict['title']}/>

        </li>
    });

    var accordianItemList =  options.map(function(oneOptionDict, index) {
        return <li className="Accordion__item">
        <LocalizedLink to={oneOptionDict['redirectTo']} children={oneOptionDict['title']}/>

        </li>
    });

    
    if (!this.props.isOpen) {
        return (
            <li className="App-nav__list__item Dropdown-trigger">
            <LocalizedLink to={this.props.redirectTo} children={this.props.title}/>
                <div className="Dropdown">
                <ul className="Dropdown-list ">
                    {optionsList}
                </ul>
                </div>
            </li>
        )   
    } else {

        return (
            <li className="App-nav__list__item">
            <LocalizedLink to={this.props.redirectTo} children={this.props.title}/>
            <div onClick={(event)=>this.showOptions(event)} className="Accordion-trigger">
            
            {this.state.is_visible ? <svg class="icon" fill="#fff" width="12px" height="12px" viewBox="0 0 1024 1024">
                <path d="M512 596.6v0 0l348.4-334.4c8.6-8.4 22.8-8.2 31.6 0.4l61.2 59.8c8.8 8.6 9 22.6 0.4 31l-425.4 408.4c-4.4 4.4-10.4 6.4-16.2 6-6 0.2-11.8-1.8-16.2-6l-425.4-408.4c-8.6-8.4-8.4-22.4 0.4-31l61.2-59.8c8.8-8.6 23-8.8 31.6-0.4l348.4 334.4z"></path>
            </svg> : <svg className="icon" fill="#fff" width="12px" height="12px" viewBox="0 0 1024 1024">
                <path d="M596.6 512v0 0l-334.4-348.2c-8.4-8.6-8.2-22.8 0.4-31.6l59.8-61.2c8.6-8.8 22.6-9 31-0.4l408.4 425.4c4.4 4.4 6.4 10.4 6 16.2 0.2 6-1.8 11.8-6 16.2l-408.4 425.2c-8.4 8.6-22.4 8.4-31-0.4l-59.8-61.2c-8.6-8.8-8.8-23-0.4-31.6l334.4-348.4z"></path>
            </svg>}
            
            </div>
            <div className="Accordion">
            <ul className={navListClass}>
                {accordianItemList}
            </ul>
            </div>
            </li>

        )
    
    }
}
}

export default NavListItem;