import React, { Component } from 'react';

class MenuButton extends Component {
  render() {
    return (
        <div className="App-header__mobile-menu-button" tabindex="1">
        <div onClick={(event)=>this.props.changeMenu(event)} className="Button Button--default Button--tiny Button--has-icon Button--white">
            <div className="Button__icon">
              <svg className="icon" fill="#004d84" width="22px" height="22px" viewBox="0 0 1024 1024">
                  <path d="M128 768h768v-85.332h-768v85.332zM128 554.668h768v-85.334h-768v85.334zM128 256v85.33h768v-85.33h-768z"></path>
              </svg>
            </div>
            <div className="Button__text blue-text">Menu</div>
        </div>
      </div>
    )
  }
}

export default MenuButton;