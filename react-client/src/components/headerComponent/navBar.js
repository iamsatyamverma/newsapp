import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import NavListItem from './navListItem.js'
import CompanyLogo from './companyLogo.js'
import { FormattedMessage } from 'react-intl';
import LocalesMenu from '../commonComponent/localeMenu.js'
import {connect} from 'react-redux';  
import {intlShape, injectIntl, defineMessages} from 'react-intl';
import LocalizedLink from '../commonComponent/localizedLink.js';
import { checkIsLoggedIn } from '../../service/utilService.js';
import { logOutUser, loadBlogs, updateSubscription } from '../../actions/index.js';
import MenuButton from './menuButton.js';

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.subscribeNow = this.subscribeNow.bind(this);

    this.changeMenu = this.changeMenu.bind(this);
    this.handleOutsideClick = this.handleOutsideClick.bind(this);

    this.state = {
      is_open: false
    }

  }

  changeMenu() {

    if (!this.state.is_open) {
      // attach/remove event handler
      document.addEventListener('click', this.handleOutsideClick, false);
    } else {
      document.removeEventListener('click', this.handleOutsideClick, false);
    }

    this.setState({is_open: !this.state.is_open})
  }

  handleOutsideClick(e) {
    // ignore clicks on the component itself
    if (this.node.contains(e.target)) {
      return;
    }
    
    this.changeMenu();
  }

  subscribeNow (event) {
    event.preventDefault()
    event.stopPropagation();
    event.nativeEvent.stopImmediatePropagation();
    this.props.dispatch(updateSubscription());
  }

  componentDidMount() {
    this.props.dispatch(loadBlogs());
  }

  onSignOut(event) {
    this.props.dispatch(logOutUser());
  }

  createLoginButton() {
    const {formatMessage} = this.props.intl;

    if (this.props.isLoggedIn){
      let userNameDiv = <div className="App-nav__list__item Dropdown-trigger">
                      <LocalizedLink className="User-nav__list__item is-selected font-size-13 Login-button" to="/profile" children={this.props.loginUser.first_name}/>
                      
                      {
                        this.state.is_open ? 
                        <div onClick={(event)=>this.onSignOut(event)}>
                        <LocalizedLink to="/pitchbook" children={formatMessage({id:"navbar.Sign_Out"})}/>
                        </div>
                        : 
                        <div className="Dropdown">
                        <ul className="Dropdown-list ">
                        <li className="Dropdown__item">
                        <LocalizedLink className="User-nav__list__item is-selected" to="/profile" children={formatMessage({id:"navbar.Profile"})}/>
                        </li>
                        <li className="Dropdown__item" onClick={(event)=>this.onSignOut(event)}>
                        <LocalizedLink to="/pitchbook" children={formatMessage({id:"navbar.Sign_Out"})}/>
                        </li>
                        </ul>
                      </div>
                      }

                    </div>
      return userNameDiv;
    }

    return <LocalizedLink className="User-nav__list__item is-selected font-size-13 Login-button" to="/login" children={formatMessage({id:"navbar.LOGIN"})}/>
  }

  render() {
    const {formatMessage} = this.props.intl;

    let accordian_class = this.state.is_open ? "App-header container-fluid is-open" : "App-header container-fluid";

    const topicOptions = [
                    // {'title':'Leadership & Management',
                    //   'redirectTo':'/topics/51/leadership-management'},
                    // {'title':'Sales Coaching &amp; Training',
                    //   'redirectTo':'/topics/54/sales-coaching-training'}
                    ];
    
    const magazineOptions = [{'title':'Current Pitchbook',
                      'redirectTo':'/pitchbook#Current-Issue'},
                    {'title':'Pitchbooks',
                      'redirectTo':'/pitchbook#PITCHBOOKS'},
                      {'title':'Featured Reports',
                      'redirectTo':'/pitchbook#FEATURED-REPORTS'}];
    
    let blogOptions = [];
    
      blogOptions = _.map(this.props.blogs, function(value, key) {
        let redirectTo = '/blog/' + value.id;
        return {title: value.header, redirectTo: redirectTo}
      });
    

    return (
        <header className={accordian_class} ref={node => { this.node = node; }}>
        <div className="App-header__inner">
        <CompanyLogo redirectTo='/' url='/static/media/logo.svg' />

        <div className="App-nav-container">
          <nav className="App-nav blue-background">
              <ul className="App-nav__list">
              {/* <h1><FormattedMessage id="navbar.hello" defaultMessage="Hello World!" description="Hello world header greeting" /></h1> */}
              <NavListItem isOpen={this.state.is_open} title={formatMessage({id:"navbar.TOPICS"})} redirectTo='/pitchbook' options={topicOptions} />
              <NavListItem isOpen={this.state.is_open} title={formatMessage({id:"navbar.PITCHBOOK"})} redirectTo='/pitchbook' options={magazineOptions} />
              <NavListItem isOpen={this.state.is_open} title={formatMessage({id:"navbar.EVENTS"})} redirectTo='/pitchbook' options={topicOptions} />
              <NavListItem isOpen={this.state.is_open} title={formatMessage({id:"navbar.VIDEOS"})} redirectTo='/pitchbook' options={topicOptions} />
              <NavListItem isOpen={this.state.is_open} title={formatMessage({id:"navbar.RESOURCES"})} redirectTo='/pitchbook' options={topicOptions} />
              <NavListItem isOpen={this.state.is_open} title={formatMessage({id:"navbar.BLOGS"})} redirectTo='/blog' options={blogOptions} />
             
              </ul>
          
          </nav>
          
          {this.state.is_open ? "": <div className="App-header__search"><div className="App-header__search__inner"><LocalesMenu/></div></div>}

           <div className="User-nav blue-background">
              <div className="User-nav__list">
              {this.state.is_open ? <div className="App-header__search"><div className="App-header__search__inner"><LocalesMenu/></div></div> : ""}

			  {this.createLoginButton()}
              <a style={{'padding': '0.8rem 2.5rem 0.3rem'}} className="Button Button--white font-size-13 PitchBook-button" href="/#/signup">{formatMessage({id:"navbar.NEWSLETTER_SIGNUP"})}</a>
              
              </div>
          </div>
        </div>

      <MenuButton changeMenu={this.changeMenu} />

    </div>
  </header>
    )
  }
}

function mapStateToProps(state, ownProps) {  
  return {
    loginUser: state.user.loginUser,
    isLoggedIn: state.user.isLoggedIn,
    blogs: state.blog.blogs
  };
}

export default connect(mapStateToProps)(injectIntl(NavBar));
