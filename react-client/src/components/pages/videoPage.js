import React, { Component } from 'react';
import VideoFrameLink from '../videoComponent/videoFrameLink';
import YoutubeVideoFrame from '../videoComponent/youtubeVideoFrame';
import VideoTileHero from '../videoComponent/videoTileHero';

class VideoPage extends Component {
  render() {
    return (
        <div>
   <div>
      <header className="Page-header container">
         <h1 className="Header Header--jumbo">Videos</h1>
      </header>
   </div>

   <div className="Section">
      <div className="container">
         <div className="Section">
            <div className="Flex__shrink Video-options">
               <div className="Video-category-dropdown">
                  <select>
                     <option value="" disabled="" hidden=""></option>
                     <option value="UUgMDZHnKV5psV66F_GokAvg" data-description="" data-title="All Categories" data-id="UUgMDZHnKV5psV66F_GokAvg">All Categories</option>
                     <option value="PLlltiIWCJGJdK3iQrsGduBgX7Eo_ooAGB" data-description="" data-title="Coaching" data-id="PLlltiIWCJGJdK3iQrsGduBgX7Eo_ooAGB">Coaching</option>
                     <option value="PLlltiIWCJGJdwXcdJCJJkluzXPzWKA7l1" data-description="" data-title="Hiring" data-id="PLlltiIWCJGJdwXcdJCJJkluzXPzWKA7l1">Hiring</option>
                     <option value="PLlltiIWCJGJct5-hPOBCEQ3rrsFA3Eod_" data-description="" data-title="Management" data-id="PLlltiIWCJGJct5-hPOBCEQ3rrsFA3Eod_">Management</option>
                     <option value="PLlltiIWCJGJeFvfvz4HKj_ARjHNYGo4PI" data-description="" data-title="Marketing" data-id="PLlltiIWCJGJeFvfvz4HKj_ARjHNYGo4PI">Marketing</option>
                     <option value="PLlltiIWCJGJd8c4-qzwlc8Q3Qo8IYRuDe" data-description="" data-title="Motivation" data-id="PLlltiIWCJGJd8c4-qzwlc8Q3Qo8IYRuDe">Motivation</option>
                     <option value="PLlltiIWCJGJc492KQit7cQVDFNaYPl1Z4" data-description="" data-title="Negotiation" data-id="PLlltiIWCJGJc492KQit7cQVDFNaYPl1Z4">Negotiation</option>
                     <option value="PLlltiIWCJGJeR7LK9lmTxIqj4j-d3YNjK" data-description="" data-title="Prospecting" data-id="PLlltiIWCJGJeR7LK9lmTxIqj4j-d3YNjK">Prospecting</option>
                     <option value="PLlltiIWCJGJd55pC7nsGp6D3uiV6_EHQX" data-description="" data-title="Sales Leadership" data-id="PLlltiIWCJGJd55pC7nsGp6D3uiV6_EHQX">Sales Leadership</option>
                     <option value="PLlltiIWCJGJcv8IYomZBIgVobvWX-ACy5" data-description="" data-title="Sales Process" data-id="PLlltiIWCJGJcv8IYomZBIgVobvWX-ACy5">Sales Process</option>
                     <option value="PLlltiIWCJGJe2EOQgjxGluragrJeV90HL" data-description="" data-title="Sales Technology &amp; CRM" data-id="PLlltiIWCJGJe2EOQgjxGluragrJeV90HL">Sales Technology &amp; CRM</option>
                     <option value="PLlltiIWCJGJfVqMipofY5xYUlv8I6h7h9" data-description="" data-title="Sales Training" data-id="PLlltiIWCJGJfVqMipofY5xYUlv8I6h7h9">Sales Training</option>
                     <option value="PLlltiIWCJGJckYoPgXTYonF2nfm_giw8R" data-description="" data-title="Selling Skills" data-id="PLlltiIWCJGJckYoPgXTYonF2nfm_giw8R">Selling Skills</option>
                     <option value="PLlltiIWCJGJd8aq53m9QObzwY0bxuWAg0" data-description="" data-title="Success Strategies" data-id="PLlltiIWCJGJd8aq53m9QObzwY0bxuWAg0">Success Strategies</option>
                  </select>
               </div>
            </div>
            
      <div>       
        <VideoTileHero videoLink="//www.youtube.com/embed/P-9iOwxHDAU" videoHeader="How to Build a Better Sales Team and Keep Top Performers" videoText="Featuring C. Lee Smith, President & CEO, SalesFuel"/>
      </div>
      
      <div className="Section__header">
          <div className="Flex"></div>
      </div>
            
            
            <div className="Video-list">
               <div className="row">
                  <div className="col-md-4">
                     <VideoFrameLink redirectTo="/videos/Hd-Z1soFwiY/winning-tips-to-get-you-to-the-decision-maker-quickly" imageLink="https://i.ytimg.com/vi/Hd-Z1soFwiY/hqdefault.jpg" title="Winning Tips to Get You to the Decision Maker Quickly"/>
                  </div>
                  
                  <div className="col-md-4">
                     <VideoFrameLink redirectTo="/videos/Hd-Z1soFwiY/winning-tips-to-get-you-to-the-decision-maker-quickly" imageLink="https://i.ytimg.com/vi/Hd-Z1soFwiY/hqdefault.jpg" title="Winning Tips to Get You to the Decision Maker Quickly"/>
                  </div>
              </div>
                  
            </div>

            <div className="row justify-content-md-center">
               <div className="col-md-4"><a className="Button Button--primary Button--block Button--large" href="/">View More Videos</a></div>
            </div>

         </div>
      </div>
   </div>
</div>
    )
  }
}

export default VideoPage;