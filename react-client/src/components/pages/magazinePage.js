import React, { Component, PropTypes } from 'react';
import PitchBookTriImage from '../pitchBookHeaderComponent/pitchbookTriImage.js'
import PitchBookHero from '../pitchBookHeaderComponent/pitchBookHero.js'
import CurrentPitchBook from '../pitchBookHeaderComponent/currentPitchBook.js'
import CurrentPitchBookSection from '../pitchBookHeaderComponent/currentPitchBookSection.js'
import TilesPitchBookSection from '../pitchBookBodyComponent/tilesPitchBookSection.js'
import {loadPitchBooks, loadTopPitchBook, updateSubscription} from '../../actions/index';
import {connect} from 'react-redux';  
import {intlShape, injectIntl, defineMessages} from 'react-intl';

class MagazinePage extends Component {
  
  constructor(props) {
    super(props);
    this.subscribeNow = this.subscribeNow.bind(this);

  }

  componentDidMount() {
    this.props.dispatch(loadPitchBooks());
    this.props.dispatch(loadTopPitchBook());
  }

  subscribeNow (event) {
    event.preventDefault()
    event.stopPropagation();
    event.nativeEvent.stopImmediatePropagation();
    this.props.dispatch(updateSubscription());
  }

  createPitchBookHeroSection(pitchbooks) {
    const {formatMessage} = this.props.intl;

    let pitchBookImages = ["/static/media/June-2018.png", "/static/media/July-2018.png"];
    const pitchBooktitleHelper = formatMessage({id:"magazinePage.Boost_your_sales_team_with"});
    const pitchBooktitle = "AfarInsights Pitchbook";
    const pitchBookInfo = formatMessage({id:"magazinePage.subscribe_now_for_dollar"});
    const pitchBookPrice = "$29/Year"

    if (pitchbooks.length > 0 ){
      pitchBookImages.splice(0, 0, pitchbooks[0].image.guid)
      return <PitchBookHero images={pitchBookImages} price={pitchBookPrice} titleHelper={pitchBooktitleHelper} title={pitchBooktitle} info={pitchBookInfo} subscribeNow={this.subscribeNow}/>
    }

    return null;
  }

  createCurrentPitchBookSection(pitchbooks) {
    let currentPitchBookSection;

    if (pitchbooks.length > 0 ){
      currentPitchBookSection = <div className="Magazine-current"> 
                                    <h3 className="blue-text Header Header--small Header--sans text-center font-size-16" style={{'font-weight': 'bold', 'margin': '.625rem 0 2rem'}}>
                                          {pitchbooks[0]['sub_articles_title']}
                                    </h3>
                                    <CurrentPitchBookSection subArticles={pitchbooks[0]['sub_articles']}/>
                                </div>
    }

    return currentPitchBookSection;
  }

render() {
    const {formatMessage} = this.props.intl;

    const PastIssuesHeader = formatMessage({id:"magazinePage.Past_Issues"});
    const SpecialIssuesHeader = formatMessage({id:"magazinePage.Special_Issues"});

    return (
        <div>
        <div className="Magazine-hero" id="Current-Issue">
        <div className="container">
           <div className="Section">
              <div className="row justify-content-center">
                 <div className="col-lg-10">
                 	{this.createPitchBookHeroSection(this.props.pitchbooks.current)}
                    {this.createCurrentPitchBookSection(this.props.pitchbooks.current)}
                 </div>
              </div>
           </div>
        </div>
     </div>
     
    <div>
      <TilesPitchBookSection pitchBooks={this.props.pitchbooks.past} sectionHeader={PastIssuesHeader}/>

      <TilesPitchBookSection pitchBooks={this.props.pitchbooks.special} sectionHeader={SpecialIssuesHeader}/>
    </div>

    </div>
    )
}
}

MagazinePage.propTypes = {  
  pitchbooks: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {  
    return {
      pitchbooks: state.pitchbook.pitchbooks
    };
  }

export default connect(mapStateToProps)(injectIntl(MagazinePage));
