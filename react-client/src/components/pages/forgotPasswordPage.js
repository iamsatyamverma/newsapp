<div class="container Container--centered">
<div class="row align-items-center justify-content-center">
<div class="col-sm-12 col-md-8 col-lg-4"><div>
    <header class="Page-header container Page-header--centered">
    <h1 class="Header Header--jumbo">Forgot Password?</h1>
    </header></div><div><div></div>
    
    <form class="Form-container">
    <fieldset>
        <div class="Form__field">
        <label>Email</label>
    <input type="text" name="email" value="" placeholder="p.venkman@sellingpower.com" />
    <span class="Form__field__error">Required</span>
    </div>
    <div>
        <button type="submit" class="Button--block Button--primary Button--block ">Reset Password</button>
        <div></div>
        </div>
        </fieldset>
        </form>
        </div>
        <div class="Login__footer"><a href="mailto:web@sellingpower.com" title="Email Selling Power Support">Need assistance? Email web@sellingpower.com</a>
        </div></div></div></div>

        