import React, { Component, PropTypes } from 'react';
import TextFormField from '../commonComponent/textFormField.js';
import { Field } from 'redux-form';
import SelectFormField from '../commonComponent/selectFormField.js';
import SignupFormCode from '../userComponent/signUpForm.js'
import {connect} from 'react-redux';  
import {registerUser} from '../../actions/index.js';
import {intlShape, injectIntl, defineMessages} from 'react-intl';
import PdfViewer from '../pdfViewer/pdfViewer.js';
import { getStringOfJson } from '../../service/utilService.js';
import { SubmissionError } from 'redux-form'
import  { Redirect } from 'react-router-dom'


class SignupPage extends Component {

  constructor (props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.submitSuccess = this.submitSuccess.bind(this);
    this.state = {toLogin:false};
  }

  handleSubmit(values){
    values['username'] = values['email'];
    return this.props.dispatch(registerUser(values));
  }

  submitSuccess(){
    this.setState({'toLogin':true});
  }

  render() {
    if (this.state.toLogin == true){
      return <Redirect to={'/login'} />;
    }

    const { handleSubmit } = this.props
    const {formatMessage} = this.props.intl;

    return (
        <div>
        <div className="Container--centered mb-5">
           <div>
              <header className="Page-header container Page-header--centered">
                 <h1 className="Header Header--jumbo">{formatMessage({id:"signupPage.Create_Your_Account"})}</h1>
              </header>
           </div>
           <div className="container">
              <div className="row align-items-center justify-content-center">
                 <div className="col-lg-8">
                    <div className="register-text text-center px-4">
                       <p>
                       {formatMessage({id:"signupPage.Create_your_Google.com_account_text"})} <b>Sales Management Digest</b> and <b>{formatMessage({id:"Daily_Boost_of_Positivity"})}</b> {formatMessage({id:"signupPage.email_newsletters"})}.
                       </p>
                    </div>
                 </div>
              </div>
              <div className="row align-items-center justify-content-center">
                 <div className="col-lg-6">
                    <div className="legal-text text-center mb-4">
                       <p>{formatMessage({id:"signupPage.google_acc_creation_alert_text"})}</p>
                    </div>
                 </div>
              </div>
              <div className="row align-items-center justify-content-center">
                 <div className="col-lg-4">
                    <div>
                       <div></div>
                       <SignupFormCode onSubmit={this.handleSubmit} handleSubmitSuccess={this.submitSuccess} />
                    </div>
                    {/* <div className="Login__footer"><a href="/privacy-policy">Privacy Policy</a><br/><a href="/login">Already a member? Sign In Here</a></div> */}
                 </div>
              </div>
           </div>
        </div>
     </div>
    )
  }
  }

function mapStateToProps(state, ownProps) {  
  return {
    signupUser: state.user.signupUser
  };
}

export default connect(mapStateToProps)(injectIntl(SignupPage));
