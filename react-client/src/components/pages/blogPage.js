import React, { Component } from 'react';
import ArticleHeader from '../articleComponent/articleHeader';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import WebinarTile from '../commonComponent/webinarTile';
import VideoTile from '../commonComponent/videoTile';
import NewsLetterTile from '../commonComponent/newsLetterTile';
import {connect} from 'react-redux';  
import {intlShape, injectIntl, defineMessages} from 'react-intl';
import { loadBlogs } from '../../actions';


class BlogPage extends Component {

  render() {
    const {blogs} = this.props;
    
    let blogId = this.props.match.params.blogId;
    
    if(!blogId) {     
      blogId = Object.keys(blogs)[0];
    }

    let renderedBlog = blogs[blogId];
    if (! renderedBlog){
      return <p/>
    }

    let header = renderedBlog.header;
    let title = renderedBlog.title.rendered;
    let authors = renderedBlog.authors;
    let imageUrl = renderedBlog.image.guid;
    let date = renderedBlog.created_date;
    let articlelink = "#"
    let content = renderedBlog.content.rendered;
    return (
    <div>
      <div className="Ad Ad--header">
      </div>
      <div className="Section">
          <div className="Article">
            <div className="container">
                <div className="row">
                  <div className="col-md-8">
                      <ArticleHeader  header={header} title={title} authors={authors} date={date} articlelink={articlelink}/>

                      <div className="Article__hero">
                        <img src={imageUrl} alt=""/>
                      </div>

                      <div className="p-relative">
                        <div className="Content-container">
                          {ReactHtmlParser(content)}
                        </div>
                      </div>
                  </div>
                  <div className="col-md-4">

                      {/* <WebinarTile /> */}
                      
                      <VideoTile /> 
                      
                      {/* <NewsLetterTile /> */}
                      
                  </div>
                </div>
            </div>
          </div>
      </div>
   {/* <div className="container mb-4">
      <div className="Ad Ad--no-background">
         <div>
            <div className="adunitContainer">
               <div id="adSlot-5" className="adBox" data-google-query-id="CL6A8uWhxd0CFZCCcAodpogH-A" style="">
                  <div id="google_ads_iframe_40692871/728x90_Leaderboard_Zone2_0__container__" style="border: 0pt none;"><iframe id="google_ads_iframe_40692871/728x90_Leaderboard_Zone2_0" title="3rd party ad content" name="google_ads_iframe_40692871/728x90_Leaderboard_Zone2_0" width="728" height="90" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" srcdoc="" style="border: 0px; vertical-align: bottom;" data-load-complete="true"></iframe></div>
               </div>
            </div>
         </div>
      </div>
      <div className="row">
         <div className="col-sm-12 col-md-8 col-lg-8 mb-5 mb-sm-0">
            <div>
               <div>
                  <div className="row mb-4">
                     <div className="col-12">
                        <a className="FeaturedArticle FeaturedArticle--hero" href="/2018/07/03/14513/a-strategic-imperative-sales-enablement-is-no-longer-an-activity-its-a-profession" style="background-image: url(&quot;https://images.unsplash.com/photo-1468814213359-09c0e70107f8?dpr=2&amp;auto=format&amp;fit=crop&amp;w=1500&amp;h=1001&amp;q=80&amp;cs=tinysrgb&amp;crop=&amp;bg=&quot;);">
                           <div className="FeaturedArticle__category Header Header--tiny">Featured Article</div>
                           <div className="FeaturedArticle__title" href="">A Strategic Imperative: Sales Enablement is no Longer an Activity, it's a Profession</div>
                        </a>
                     </div>
                  </div>
               </div>
               <div className="row">
                  <div className="col-md-6 mb-4 mb-md-0">
                     <a className="FeaturedArticle" href="/2018/06/15/14470/new-ways-to-make-sales-training-more-engaging-and-effective" style="background-image: url(&quot;https://images.unsplash.com/photo-1468956398224-6d6f66e22c35?dpr=2&amp;auto=format&amp;fit=crop&amp;w=1500&amp;h=994&amp;q=80&amp;cs=tinysrgb&amp;crop=&amp;bg=&quot;);">
                        <div className="FeaturedArticle__title" href="">New Ways to Make Sales Training More Engaging and Effective</div>
                     </a>
                  </div>
                  <div className="col-md-6 mb-4 mb-md-0">
                     <a className="FeaturedArticle" href="/2015/03/06/10459/7-tips-to-recruit-better-salespeople" style="background-image: url(&quot;https://images.unsplash.com/photo-1468956398224-6d6f66e22c35?dpr=2&amp;auto=format&amp;fit=crop&amp;w=1500&amp;h=994&amp;q=80&amp;cs=tinysrgb&amp;crop=&amp;bg=&quot;);">
                        <div className="FeaturedArticle__title" href="">7 Tips to Recruit Better Salespeople</div>
                     </a>
                  </div>
               </div>
            </div>
         </div>
         <div className="col-sm-12 col-md-4 col-lg-4">
            <div className="PopularArticlesListSection">
               <div className="Divider"></div>
               <h3 className="Header--tiny">Popular Articles</h3>
               <ul className="DividedList">
                  <li className="DividedList__item"><a href="/2018/08/21/14698/how-to-stop-losing-sales-on-price/">How to Stop Losing Sales on Price</a></li>
                  <li className="DividedList__item"><a href="/2011/09/08/9598/great-ways-to-wrap-up-a-presentation/">Great Ways to Wrap Up a Presentation</a></li>
                  <li className="DividedList__item"><a href="/2018/08/14/14686/you-made-the-sale-but-are-you-leaking-the-revenue/">You Made the Sale, but Are You Leaking the Revenue?</a></li>
                  <li className="DividedList__item"><a href="/2011/02/04/9383/stop-talking-about-yourself-and-start-selling-more/">Stop Talking About Yourself (and Start Selling More)</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div> */}
  </div>
    )
  }
}

function mapStateToProps(state, ownProps) {  
  return {
    blogs: state.blog.blogs
  };
}

export default connect(mapStateToProps)(injectIntl(BlogPage));