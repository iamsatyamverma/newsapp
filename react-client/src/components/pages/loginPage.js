import React, { Component, PropTypes } from 'react';
import TextFormField from '../commonComponent/textFormField.js';
import { Field } from 'redux-form';
import SignupFormCode from '../userComponent/signUpForm.js'
import {connect} from 'react-redux';  
import LoginFormCode from '../userComponent/loginForm.js';
import { logInUser } from '../../actions/index.js';
import { Redirect } from 'react-router'
import {intlShape, injectIntl, defineMessages} from 'react-intl';
import LocalizedLink from '../commonComponent/localizedLink.js';
import { SubmissionError } from 'redux-form'
import { checkIsLoggedIn } from '../../service/utilService.js';


class LoginPage extends Component {

  constructor (props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {} 
  }

  handleSubmit(values){
    return this.props.dispatch(logInUser(values))
  }

  render() {
    const {formatMessage} = this.props.intl;

    if (checkIsLoggedIn()) {
      return <Redirect to={'/profile'} />;
    }

    const { handleSubmit } = this.props
    return (
        <div className="container Container--centered mb-5">
        <div className="row justify-content-center align-items-center">
           <div className="col-sm-12 col-md-8 col-lg-4">
              <div>
                 <header className="Page-header container Page-header--centered">
                    <h1 className="Header Header--jumbo">{formatMessage({id:"loginPage.Login"})}</h1>
                 </header>
              </div>

              <LoginFormCode onSubmit={this.handleSubmit}/>

              <div className="Login__footer">
              <LocalizedLink to="/signup" children={formatMessage({id:"loginPage.Need_Account_Register"})}/>
              </div>
           </div>
        </div>
     </div>
    )
  }
  }

function mapStateToProps(state, ownProps) {  
  return {
    loginUser: state.user.loginUser
  };
}

export default connect(mapStateToProps)(injectIntl(LoginPage));
