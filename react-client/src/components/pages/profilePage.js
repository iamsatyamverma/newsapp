import React, { Component } from 'react';
import ProfileFormCode from '../userComponent/profileForm';
import { loadUserProfile, updateUserProfile } from '../../actions/index';
import {connect} from 'react-redux';  
import {intlShape, injectIntl, defineMessages} from 'react-intl';


class ProfilePage extends Component {
  
  constructor (props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.submitSuccess = this.submitSuccess.bind(this);
    this.state = {updatedProfile:false};
  }

  componentDidMount() {
    this.props.dispatch(loadUserProfile());
  }

  handleSubmit(values) {
    this.setState({'updatedProfile':false});
    values['username'] = values['email'];
    return this.props.dispatch(updateUserProfile(values));
  }

  submitSuccess() {
    this.setState({'updatedProfile':true});
  }

  render() {
    const { handleSubmit } = this.props
    const {formatMessage} = this.props.intl;

    return (
        <div>
   <div className="Section">
      <div>
         <div>
            <header className="Page-header container">
               <h1 className="Header Header--jumbo">Your Profile</h1>
            </header>
         </div>
         <div className="Section__body container">
            <div>
               <div></div>
               <ProfileFormCode onSubmit={this.handleSubmit} handleSubmitSuccess={this.submitSuccess}/>
               {this.state.updatedProfile?<div className="Alert Alert--success">{formatMessage({id:"profilePage.Profile_updated_successfully"})}</div>:''}
            </div>
         </div>
      </div>
   </div>
</div>
    )
  }
}

function mapStateToProps(state, ownProps) {  
  return {
    profileUser: state.user.profileUser
  };
}

export default connect(mapStateToProps)(injectIntl(ProfilePage));
