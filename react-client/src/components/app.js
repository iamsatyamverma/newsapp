import React, { Component } from 'react';
import { HashRouter as Router, Route, Link, Redirect} from 'react-router-dom';
import { browserHistory } from 'react-router';
import HomePage from './pages/homePage.js';
import MagazinePage from './pages/magazinePage.js';
import SignupPage from './pages/signupPage.js';
import NavBar from './headerComponent/navBar.js';
import Footer from './footerComponent/footer.js';
import {loadPitchBooks} from '../actions/index';
import {Provider} from 'react-redux';
import configureStore from '../store/index';
import LoginPage from './pages/loginPage.js';
import { IntlProvider } from 'react-intl';
import messages from "../locale/dictionary.js"
import { getLocale } from '../service/utilService.js';
import ProfilePage from './pages/profilePage.js';
import BlogPage from './pages/blogPage.js';
import PdfViewer from './pdfViewer/pdfViewer.js';
import VideoPage from './pages/videoPage.js';

const store = configureStore();

window.store = store;

class App extends Component {
  
  render() {

  var locale = getLocale()
  
  const NoMatch = ({ location }) => (
    // <div>
      <Redirect from='*' to="/pitchbook"/>
    // </div>
  )

  return (
      <IntlProvider locale={locale} messages={messages(locale)}>
      <Provider store={store}>
      <Router>
        <div className="App">
          <NavBar />
          {/* <Route name="home" exact path="/" component={HomePage} /> */}
          <Route name="pitchbook" exact path="/pitchbook" component={MagazinePage} />
          <Route name="ProfilePage" exact path="/profile" component={ProfilePage} />
          <Route name="SignupPage" exact path="/signup" component={SignupPage} />
          <Route name="LoginPage" exact path="/login" component={LoginPage} />

          <Route name="BlogPage" exact path='/blog' component={BlogPage} />
          <Route name="BlogPage" exact path='/blog/:blogId' component={BlogPage} />
          
          {/* <Route name="VideoPage" exact path='/video' component={VideoPage} /> */}

          <Route path='*' exact={true} component={NoMatch} />
          {/* <Route name="Pdf" exact path='/pdf' render={(props) => <PdfViewer {...props} url='https://mozilla.github.io/pdf.js/web/compressed.tracemonkey-pldi-09.pdf' />} /> */}
          <Footer />
        </div>
      </Router>
      </Provider>
      </IntlProvider>
    )
  }
}

export default App;