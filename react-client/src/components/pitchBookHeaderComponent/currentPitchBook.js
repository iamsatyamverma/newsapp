import React, { Component } from 'react';

class CurrentPitchBook extends Component {
  render() {
    return (
    <div className="Magazine-tile">
        <div className="media">
           <img src={this.props.imageUrl} className="Magazine-tile__image" alt="" />
           <div className="media-body Magazine-tile__content">
              <h2 className="Magazine-tile__title Header--medium"><a href={this.props.redirectTo} target="_blank">{this.props.title}</a></h2>
           </div>
        </div>
     </div>
    )
  }
}

export default CurrentPitchBook;