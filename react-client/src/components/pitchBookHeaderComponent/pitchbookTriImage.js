import React, { Component } from 'react';

class PitchBookTriImage extends Component {
  render() {
    return (
      <div className="Magazine-images">
        <div>
        <img src={this.props.imageUrl1} alt="Image" />
        <img src={this.props.imageUrl2} alt="July 2018" />
        <img src={this.props.imageUrl3} alt="June 2018" />
        </div>
      </div>
    )
  }
}

export default PitchBookTriImage;