
import React, { Component } from 'react';
import PitchBookTriImage from './pitchbookTriImage.js'
import {intlShape, injectIntl, defineMessages} from 'react-intl';

class PitchBookHero extends Component {
  render() {
    const {formatMessage} = this.props.intl;

    return (
        <div className="row">
        <div className="col-lg-4">
           
           <PitchBookTriImage imageUrl1={this.props.images[0]} imageUrl2={this.props.images[1]} imageUrl3={this.props.images[2]} />
       
        </div>
        <div style={{'padding-left': '1.8rem'}} className="col-lg-8">
           <div className="Flex Flex--center">
              <div>
                 <h2 className="Header--medium mb-0 blue-text" style={{'font-style': 'italic'}}>{this.props.titleHelper}</h2>
                 <h1 className="Header--jumbo mb-3 blue-text font-size-25 margin-bottom-0-6">{this.props.title}</h1>
                 <p className="mb-4 pb-2 pitchbook-info">
                    {this.props.info}       
                 </p>
                 <div>
                    <div className="Magazine-button-container">
                       <a className="Button Button--primary Button--large d-block d-sm-inline font-size-10" onClick={(event)=>this.props.subscribeNow(event)} href="#">
                          <span className="text--light">{formatMessage({id:"pitchBookHero.Subscribe_Only"})} {this.props.price}</span>
                       </a>
                       <a className="Button Button--neutral--outline Button--large d-block d-sm-inline ml-0 ml-md-3 mt-3 mt-sm-0 font-size-10" href="/pitchbook/sample">{formatMessage({id:"pitchBookHero.Get_Sample_Issue"})}</a>
                    </div>
                 </div>
              </div>
           </div>
        </div>
       </div>
    )
  }
}

export default (injectIntl(PitchBookHero));





