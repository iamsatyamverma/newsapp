import React, { Component, PropTypes } from 'react';
import CurrentPitchBook from './currentPitchBook.js'
class CurrentPitchBookSection extends Component {

  // createPitchBooTable(pitchBookList, startIndex){
  //   var columnpitchBookList = []
  //   for (let i=startIndex; i<pitchBookList.length; i+=2){
  //     columnpitchBookList.push(<CurrentPitchBook imageUrl={pitchBookList[i].imageUrl} redirectTo={pitchBookList[i].redirectTo} title={pitchBookList[i].title}/>)
  //   }    
  //   return columnpitchBookList;
  // }

  createSubArticleTable(subArticleList, startIndex){
    var columnsubArticleList = []
    if (subArticleList){
      for (let i=startIndex; i<subArticleList.length; i+=2){
        columnsubArticleList.push(<CurrentPitchBook imageUrl={subArticleList[i].thumbnail.guid} redirectTo={subArticleList[i].document_link} title={subArticleList[i].post_title}/>)
      }  
    }  
    return columnsubArticleList;
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-6">
        {this.createSubArticleTable(this.props.subArticles, 0)}
        </div>
        <div className="col-md-6">
        {this.createSubArticleTable(this.props.subArticles, 1)}
        </div>
        </div>
    )
  }
}

CurrentPitchBookSection.propTypes = {  
  subArticles: PropTypes.array.isRequired
};

export default CurrentPitchBookSection;