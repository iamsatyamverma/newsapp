import React, { Component } from 'react';
import PitchBookTile from './pitchBookTile.js'

class TilesPitchBookSection extends Component {
  render() {
    return (
        <div className="Section Section--large container">
        <div style={{float: 'none', margin: '0 auto', padding: '0px'}} className="col-lg-10">
           <div className="Divider"></div>
           <h2 className="Header--large font-size-18" id={this.props.sectionHeader.split(' ').join('-')}>{this.props.sectionHeader}</h2>
           <div className="row">
           {this.props.pitchBooks.map((pitchBook) => 
                        <PitchBookTile imageUrl={pitchBook['image']['guid']} title={pitchBook['title']['rendered']} redirectTo={pitchBook['document_link']}/>
                      )}
            </div>
        </div> 
        </div>
    )
  }
}

export default TilesPitchBookSection;