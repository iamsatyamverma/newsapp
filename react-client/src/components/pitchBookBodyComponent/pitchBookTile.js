import React, { Component } from 'react';

class PitchBookTile extends Component {
  render() {
    return (
    <div className="col-6 col-md-4 col-lg-3">
        <a href={this.props.redirectTo} target="_blank" className="Magazine-issue">
           <img src={this.props.imageUrl} alt="July 2018"/>
           <div className="Magazine-issue__title">{this.props.title}</div>
        </a>
     </div>
    )
  }
}

export default PitchBookTile;