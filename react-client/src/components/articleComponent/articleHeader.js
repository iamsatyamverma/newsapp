import React, { Component } from 'react';
import LocalizedLink from '../commonComponent/localizedLink';

class ArticleHeader extends Component {
  render() {
    return (
        <header className="Article__header">
            <LocalizedLink className="Header Header--tiny" to={this.props.articlelink} children={this.props.header}/>
            <h1 className="Header Header--jumbo Article__title">{this.props.title}</h1>
            <div className="Section__header__meta">
            <span className="Header Header--tiny">
            
            {this.props.authors.map(function(author, index){
                let amp = index ? " & ": "";           
                return amp + author;
            })}
             &nbsp;&nbsp;.&nbsp;&nbsp;
            </span>
            <span className="Header Header--tiny">{this.props.date}</span>
            </div>
        </header>
    )
  }
}

export default ArticleHeader;