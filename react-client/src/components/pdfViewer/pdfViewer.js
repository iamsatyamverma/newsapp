import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './pdfViewer.css';
import Viewer from './viewer.js';
import Toolbar from './toolBar.js';
import pdfjsLib from 'pdfjs-dist/webpack';

class PdfViewer extends Component {
  componentDidMount() {
    let loadingTask = pdfjsLib.getDocument(this.props.url);
    loadingTask.promise.then((doc) => {
      console.log(`Document ${this.props.url} loaded ${doc.numPages} page(s)`);
      this.viewer.setState({
        doc,
      });
    }, (reason) => {
      console.error(`Error during ${this.props.url} loading: ${reason}`);
    });
  }
  zoomIn(e) {
    this.viewer.setState({
      scale: this.viewer.state.scale * 1.1
    });
  }
  zoomOut(e) {
    this.viewer.setState({
      scale: this.viewer.state.scale / 1.1
    });
  }
  displayScaleChanged(e) {
    this.toolbar.setState({
      scale: e.scale
    });
  }
  render() {
    return (
      <div className="Pdf-App">
        <div className="Pdf-App-header">
          <h2>Welcome to PDF.js</h2>
        </div>
        <Toolbar
          ref={(ref) => this.toolbar = ref} 
          onZoomIn={(e) => this.zoomIn(e)}
          onZoomOut={(e) => this.zoomOut(e)}></Toolbar>
        <div className="Pdf-App-body">
          <Viewer
            ref={(ref) => this.viewer = ref}
            onScaleChanged={(e) => this.displayScaleChanged(e)}></Viewer>
        </div>
      </div>
    );
  }
}

PdfViewer.propTypes = {
  url: PropTypes.string, 
};

export default PdfViewer;