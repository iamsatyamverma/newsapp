import React from 'react';
import { Field, reduxForm } from 'redux-form';
import TextFormField from '../commonComponent/textFormField.js'
import SelectFormField from '../commonComponent/selectFormField.js';
import { jobTitleList, revenueList, countryList } from '../../constants/signup-options.js';
import {intlShape, injectIntl, defineMessages} from 'react-intl';

const validate = values => {
    const errors = {}
    if (!values.first_name) {
      errors.first_name = 'Required'
    } else if (values.first_name.length < 2) {
      errors.first_name = 'Minimum be 2 characters or more'
    }

    if (!values.email) {
      errors.email = 'Required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }

    if (!values.job_title) {
        errors.job_title = 'Required'
      }

    if (!values.country) {
        errors.country = 'Required'
      }

      if (!values.revenue) {
        errors.revenue = 'Required'
      }

      if (!values.company) {
        errors.company = 'Required'
      } else if (values.company.length < 2) {
        errors.company = 'Minimum be 2 characters or more'
      }

    return errors
  }


let SignupFormCode = props => {
  const { handleSubmit, pristine, submitting, submitFailed, error } = props;
  const {formatMessage} = props.intl;

  return (
        <form className="Form-container" onSubmit={ handleSubmit }>
        <fieldset>
           <div className="Register-form step-1">
              <div className="Register-form step-1">
                 <div className="Form-tab Form-tab--1">

                  <Field label={formatMessage({id:"signupForm.Email"})} name="email" component={TextFormField}  placeholder="john.doe@example.com"/>
                  <Field label={formatMessage({id:"signupForm.First_Name"})} name="first_name" component={TextFormField} placeholder="John" />
                  <Field label={formatMessage({id:"signupForm.Company"})} name="company" component={TextFormField} placeholder="Company Name" />
                  <Field label={formatMessage({id:"signupForm.Job_Title"})} name="job_title" component={SelectFormField} options={jobTitleList} />
                  <Field label={formatMessage({id:"signupForm.Revenue"})} name="revenue" component={SelectFormField} options={revenueList} />
                  <Field label={formatMessage({id:"signupForm.Country"})} name="country" component={SelectFormField} options={countryList} />
                  <Field type="password" label={formatMessage({id:"signupForm.Password"})} component={TextFormField} name="password" placeholder="********" />

                 </div>
              </div>
           </div>
        <div>
            <button type="submit" className="Button--block Button--primary Button--block disabled={pristine || submitting">{!submitting?formatMessage({id:"signupForm.Submit"}): formatMessage({id:"signupForm.Submitting"})}</button>

            {submitFailed? <div className="row subscribe-alert">
            <div className="col-md-12">
                <div className="Alert Alert--error">{error}</div>
            </div>
            </div> : ""}
        </div>
           
        </fieldset>
      </form>
    )
  }

  SignupFormCode = reduxForm({
    form: 'signUp',
    validate,
    onSubmitSuccess: (result, dispatch, props) => {
      props.handleSubmitSuccess()
    }
  })(SignupFormCode);
  
  
  export default injectIntl(SignupFormCode);
