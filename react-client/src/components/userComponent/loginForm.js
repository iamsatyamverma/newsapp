import React from 'react';
import { Field, reduxForm } from 'redux-form';
import TextFormField from '../commonComponent/textFormField.js'
import SelectFormField from '../commonComponent/selectFormField.js';
import {intlShape, injectIntl, defineMessages} from 'react-intl';

const validate = values => {
    const errors = {}

    if (!values.username) {
      errors.username = 'Required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.username)) {
      errors.username = 'Invalid email address'
    }
    
    return errors
  }

let LoginFormCode = props => {
    const { handleSubmit, pristine, submitting, submitFailed } = props;
    const {formatMessage} = props.intl;

    return (
        <form className="Form-container" onSubmit={ handleSubmit }>
            <fieldset>
            <div className="row">
                <div className="col-12">    
                    <Field label={formatMessage({id:"loginForm.Email"})} name="username" component={TextFormField}  placeholder=""/>
                </div>
                <div className="col-12">
                    <Field type="password" label={formatMessage({id:"loginForm.Password"})} component={TextFormField} name="password" placeholder="********" />
                </div>
                <div className="col-12">
                    <button type="submit" className="Button Button--primary Button--block disabled={pristine || submitting" >{!submitting?formatMessage({id:"loginForm.Sign_In"}): formatMessage({id:"loginForm.Signing_In"})}</button>
                {submitFailed? <div className="subscribe-alert">
                    <div className="Alert Alert--error">{formatMessage({id:"loginForm.username_password_mismatch"})}</div>
                </div> : ""}

                </div>
            </div>
            </fieldset>
        </form>
    )
  }

  LoginFormCode = reduxForm({
    form: 'login',
    validate,
  })(LoginFormCode);
  
  
  export default injectIntl(LoginFormCode);
