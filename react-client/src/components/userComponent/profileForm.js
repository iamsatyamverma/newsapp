import React from 'react';
import { Field, reduxForm } from 'redux-form';
import TextFormField from '../commonComponent/textFormField.js'
import SelectFormField from '../commonComponent/selectFormField.js';
import { jobTitleList, revenueList, countryList } from '../../constants/signup-options.js';
import {intlShape, injectIntl, defineMessages} from 'react-intl';
import {connect} from 'react-redux';  

const validate = values => {
    const errors = {}
    if (!values.first_name) {
      errors.first_name = 'Required'
    } else if (values.first_name.length < 2) {
      errors.first_name = 'Minimum be 2 characters or more'
    }

    if (!values.email) {
      errors.email = 'Required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }

    if (!values.job_title) {
        errors.job_title = 'Required'
      }

    if (!values.country) {
        errors.country = 'Required'
      }

      if (!values.revenue) {
        errors.revenue = 'Required'
      }

      if (!values.company) {
        errors.company = 'Required'
      } else if (values.company.length < 2) {
        errors.company = 'Minimum be 2 characters or more'
      }

    return errors
  }


let ProfileFormCode = props => {
  
  const { handleSubmit, pristine, submitting, submitFailed, error } = props;
    const {formatMessage} = props.intl;

    return (
        <form className="" onSubmit={ handleSubmit }>
        <fieldset>
           <div>
           
            <div className="row">
                <div className="col-lg-6 mb-5 mb-sm-3">
                    <Field label={formatMessage({id:"profileForm.First_Name"})} name="first_name" component={TextFormField} placeholder="First Name" />
                    <Field label={formatMessage({id:"profileForm.Last_Name"})} name="last_name" component={TextFormField} placeholder="Last Name" />
                    <Field label={formatMessage({id:"profileForm.Email"})} name="email" component={TextFormField}  placeholder="john.doe@example.com"/>

                    <Field label={formatMessage({id:"profileForm.Company"})} name="company" component={TextFormField} placeholder="Company Name" />
                    <Field label={formatMessage({id:"profileForm.Job_Title"})} name="job_title" component={SelectFormField} options={jobTitleList} />
                    <Field label={formatMessage({id:"profileForm.Country"})} name="country" component={SelectFormField} options={countryList} />
                    <Field label={formatMessage({id:"profileForm.Revenue"})} name="revenue" component={SelectFormField} options={revenueList} />
                    {/* <Field type="password" label="Password" component={TextFormField} name="password" placeholder="••••••••" /> */}
                </div>
            </div>
           </div>
        
        <div className="row justify-content-center">
            <div className="col-sm-6">
            <div>
                <button type="submit" className="Button--block Button--primary Button--block disabled={pristine || submitting">{!submitting?formatMessage({id:"profileForm.Update_Profile"}): formatMessage({id:"profileForm.Updating_Profile"})}</button>
                
                {submitFailed? <div className="row subscribe-alert">
            <div className="col-md-12">
                <div className="Alert Alert--error">{error}</div>
            </div>
            </div> : ""}

            </div>
            </div>
        </div>
           
        </fieldset>
      </form>
    )
  }

  const mapStateToProps = (state, props) => ({    
    initialValues: state.user.profileUser
  })


  ProfileFormCode = reduxForm({
    form: 'profile',
    validate,
    onSubmitSuccess: (result, dispatch, props) => {
      props.handleSubmitSuccess()
    },
    enableReinitialize : true,
  })(ProfileFormCode);

  ProfileFormCode = connect(
    mapStateToProps,
    )(ProfileFormCode);
  
  export default injectIntl(ProfileFormCode);