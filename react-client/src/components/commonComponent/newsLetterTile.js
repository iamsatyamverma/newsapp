
import React, { Component } from 'react';

class NewsLetterTile extends Component {
  render() {
    return (
        <a href="https://www.sellingpower.com/newsletters?sp_src=SP-Newsletter-Tile" className="Newsletter-tile" target="_blank" rel="noopener noreferrer">
            <h3 className="Header Header--tiny">SALES MANAGEMENT DIGEST</h3>
            <div className="Divider Divider--thin"></div>
            <h4 className="Header Header--medium">Get insight, strategies, and best-practices for Sales Leaders, delivered weekly.</h4>
            <p className="text-bold">Subscribe now.</p>
            <div className="form-group"></div>
        </a>
    )
  }
}

export default NewsLetterTile;