import React, { Component } from 'react';

class SocialLinks extends Component {
  render() {
    return (
        <div className="App-footer__social-icons">
         <a className="ion-social-facebook" href="https://www.facebook.com/Afar-Insights-1871894016258345/?modal=admin_todo_tour" target="_blank" rel="noopener noreferrer">
            <svg className="" fill="white" width="18px" height="18px" viewBox="0 0 1024 1024">
               <path d="M576 384v-76.2c0-34.4 7.6-51.8 61-51.8h67v-128h-111.8c-137 0-182.2 62.8-182.2 170.6v85.4h-90v128h90v384h166v-384h112.8l15.2-128h-128z"></path>
            </svg>
         </a>
         <a className="ion-social-twitter" href="https://twitter.com/Afar_Insights?lang=en" target="_blank" rel="noopener noreferrer">
            <svg className="" fill="white" width="16px" height="16px" viewBox="0 0 1024 1024">
               <path d="M984 219c-34.8 15.4-72 25.8-111.2 30.6 40-24 70.8-62 85.2-107.2-37.4 22.2-78.8 38.4-123 47-35.4-37.8-85.8-61.4-141.4-61.4-107 0-193.6 86.8-193.6 193.8 0 15.2 1.6 30 5 44.2-161-8-303.8-85.2-399.2-202.6-16.6 28.6-26.2 62-26.2 97.4 0 67.2 34.4 126.6 86.4 161.4-32-0.8-62-9.6-88-24.2 0 0.8 0 1.6 0 2.4 0 94 66.8 172.2 155.4 190-16.2 4.4-33.4 6.8-51 6.8-12.4 0-24.6-1.2-36.4-3.6 24.6 77 96.2 133 181 134.6-66.2 52-149.8 83-240.6 83-15.6 0-31-1-46.2-2.8 85.4 55.6 187.2 87.6 296.4 87.6 356.6 0 551.4-295.4 551.4-551.6 0-8.4-0.2-16.8-0.6-25 37.8-27.4 70.6-61.4 96.6-100.4z"></path>
            </svg>
         </a>
         <a className="ion-social-linkedin" href="https://www.linkedin.com/company/afar-insights" target="_blank" rel="noopener noreferrer">
            <svg className="" fill="white" width="16px" height="16px" viewBox="0 0 1024 1024">
               <path d="M834.4 128h-640.8c-35 0-65.6 25.2-65.6 59.8v642.2c0 34.8 30.6 65.8 65.6 65.8h640.6c35.2 0 61.6-31.2 61.6-65.8v-642.2c0.2-34.6-26.4-59.8-61.4-59.8zM366 768h-110v-342h110v342zM314.8 374h-0.8c-35.2 0-58-26.2-58-59 0-33.4 23.4-59 59.4-59s58 25.4 58.8 59c0 32.8-22.8 59-59.4 59zM768 768h-110v-187c0-44.8-16-75.4-55.8-75.4-30.4 0-48.4 20.6-56.4 40.6-3 7.2-3.8 17-3.8 27v194.8h-110v-342h110v47.6c16-22.8 41-55.6 99.2-55.6 72.2 0 126.8 47.6 126.8 150.2v199.8z"></path>
            </svg>
         </a>
      </div>
    )
  }
}

export default SocialLinks;