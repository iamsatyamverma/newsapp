import React, { Component } from 'react';

class WebinarTile extends Component {
  render() {
    return (
        <a href="https://www.sellingpower.com/webinars2/2018/09/20/sp/?sp_src=SP-Webinar-Tile" className="Webinar-tile" target="_blank" rel="noopener noreferrer">
            <h3 className="Header Header--tiny">Free Webinar</h3>
            <div className="Divider Divider--thin"></div>
            <h4 className="Header Header--medium">How to Achieve Predictable Revenue Growth</h4>
            <p>
                Date: Thursday, September 20<br/>Time: 2:00 p.m. ET
            </p>
            <div className="form-group"></div>
        </a>
    )
  }
}

export default WebinarTile;