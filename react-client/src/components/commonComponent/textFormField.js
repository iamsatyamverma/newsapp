import React from 'react';

export const TextFormField = ({ input, label, type, placeholder, meta: { touched, error, warning } }) => {
  console.log('inputStuff: ', input);
  return (
    <div className="Form__field">
         <label>
          {label}
         </label>
         <input {...input} placeholder={placeholder} type={type} />
         {touched && ((error && <span className="Form__field__error">{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
  );
}

export default TextFormField;