import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { checkIsLoggedIn, postfixLocale } from '../../service/utilService';

const PrivateRoute = ({ component: Component, ...rest }) => {

  // Add your own authentication on the below line.
    const isLoggedIn = checkIsLoggedIn()
  return (
    <Route
      {...rest}
      render={props =>
        {
        let pathname = postfixLocale('/login');
        !isLoggedIn ? (
          <Component {...props} />
        ) : (
          <Redirect to={{pathname: '/login', state: {from: props.location}}}/>
        )
    }
      }
    />
  )
}

export default PrivateRoute