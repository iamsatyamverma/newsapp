import React, {Component} from 'react';
import {intlShape, injectIntl, defineMessages} from 'react-intl';
import { setLocale, getLocale } from '../../service/utilService';

class LocalesMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: getLocale()
        }
      }

    handleClick(locale){        
        return setLocale(locale);
    }

    render() {
        const {formatMessage} = this.props.intl;

        return (
            <select value={this.state.value} onChange={(event)=>this.handleClick(event.target.value)}>
                <option value="en" name="locale">EN</option>
                <option value="zh" name="locale">ZH</option>
            </select>
            // <div className="Locale--header blue-background">
            //     <button children="English" className="Button Button--primary background-gray Button--locale" onClick={()=>this.handleClick('en')} />

            //     <button disabled children="普通话" className="Button Button--primary background-gray Button--locale" onClick={()=>this.handleClick('zh')} />                    
            // </div>

        );
    }
}

LocalesMenu.propTypes = {
    intl: intlShape.isRequired,
};

export default injectIntl(LocalesMenu);