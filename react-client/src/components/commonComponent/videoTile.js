import React, { Component } from 'react';

class VideoTile extends Component {
  render() {
    return (
        <a className="Video-tile Video-tile--card" href="https://www.youtube.com/watch?v=akFsTDv3kHo">
            <div className="Video-tile__image-container">
            <div className="Video-tile__image"><img src="/static/media/video-image.png" alt="Power Metals Corp."/></div>
            </div>
            <div className="Video-tile__content">
            <div className="Header Header--tiny"></div>
            <h3 className="Header Header--medium">Power Metals Corp.</h3>
            </div>
        </a>
    )
  }
}

export default VideoTile;