import { connect } from 'react-redux'
import { HashLink as Link } from 'react-router-hash-link';

import React, { Component } from 'react'
import { getLocale } from '../../service/utilService';


class LocalizedLink extends Component {
    render() {
        const { to, locale, className, children } = this.props

        return (
            <Link
            
            scroll={el => el.scrollIntoView({ behavior: 'smooth', block: 'start' })}

                // onClick={onClick}
                className={className}
                to={to}
            >
                {children}
            </Link>
        )
    }
}

export default connect(
    state => ({ locale: getLocale()})
)(LocalizedLink)