import React from 'react';

export const SelectFormField = ({ input, label, options, meta: { touched, error, warning } }) => {
  console.log('inputStuff: ', input);
  return (

    <div className="Form__field">
         <div>
            <label for="title">{label}</label>
            <div>
               <select {...input}>
                  {options.map((option) => <option key={option.label} value={option.value} disabled="">{option.label}</option>)}
               </select>
               {touched && ((error && <span className="Form__field__error">{error}</span>) || (warning && <span>{warning}</span>))}

            </div>
         </div>
     </div>
  );
}

export default SelectFormField;