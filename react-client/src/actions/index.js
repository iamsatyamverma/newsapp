import { ADD_ARTICLE, userConstants, pitchbookConstants, blogConstants } from "../constants/action-types";
import pitchBookApi from '../api/pitchBookApi';
import { createLoginSession, removeLoginSession, getStringOfJson } from "../service/utilService";
import { SubmissionError } from 'redux-form'

export function loadPitchBookSuccess(pitchBooks) {  
    return {type: pitchbookConstants.LOAD_PITCHBOOKS_SUCCESS, pitchBooks};
  }

export function loadBlogsSuccess(blogs) {  
    return {type: blogConstants.LOAD_BLOGS_SUCCESS, blogs};
  }

export function loadTopPitchBookSuccess(pitchBooks) {
    return {type: pitchbookConstants.LOAD_TOP_PITCHBOOKS_SUCCESS, pitchBooks};
  }

  export function loadUserProfileSuccess(user) {
    return {type: userConstants.PROFILE_REQUEST_SUCCESS, user};
  }

export function loadUserRegisterSuccess(user) {  
    return {type: userConstants.REGISTER_SUCCESS, user};
  }

export function updateProfileSuccess(user){
  return {type: userConstants.PROFILE_UPDATE_SUCCESS, user};
}

export function loginRequest() {  
  return {type: userConstants.LOGIN_REQUEST}
  }
  
export function loginSuccess(user) {  
    return {type: userConstants.LOGIN_SUCCESS, user}
  }

export function loginFailed() {  
    return {type: userConstants.LOGIN_FAILURE}
  }

export function logout() {  
    return {type: userConstants.LOGOUT}
  }

export function updateSubscriptionSuccess(response) {
  return {type: userConstants.SUBSCRIPTION_UPDATE_SUCCESS, response}
}

export function loadPitchBooks() {  
  return function(dispatch) {
    return pitchBookApi.getAllPitchBooks().then(pichbooks => {
      dispatch(loadPitchBookSuccess(pichbooks));
    }).catch(error => {
      throw(error);
    });
  };
}

export function loadTopPitchBook() {  
  return function(dispatch) {
    return pitchBookApi.getTopPitchBook().then(pichbooks => {
      dispatch(loadTopPitchBookSuccess(pichbooks));
    }).catch(error => {
      throw(error);
    });
  };
}

export function loadBlogs() {  
  return function(dispatch) {
    return pitchBookApi.getAllBlogs().then(blogs => {
      dispatch(loadBlogsSuccess(blogs));
    }).catch(error => {
      throw(error);
    });
  };
}

export function loadUserProfile() {  
  return function(dispatch) {
    return pitchBookApi.getUserProfile().then(user => {
      dispatch(loadUserProfileSuccess(user));
    }).catch(error => {
      throw(error);
    });
  };
}

export function registerUser(user) {  
  return function(dispatch) {
    return pitchBookApi.register(user).then(user => {
      dispatch(loadUserRegisterSuccess(user));
      
    }).catch(error => {           
      throw new SubmissionError({
        _error: getStringOfJson(error)
      })
    });
  };
}

export function updateUserProfile(user) {  
  return function(dispatch) {
    return pitchBookApi.updateProfile(user).then(user => {
      dispatch(updateProfileSuccess(user));
      
    }).catch(error => {           
      throw new SubmissionError({
        _error: getStringOfJson(error)
      })
    });
  };
}

export function logInUser(credentials) {  
  return function(dispatch) {
    dispatch(loginRequest());
    return pitchBookApi.login(credentials).then(response => {
      createLoginSession(response.user, response.token)
      dispatch(loginSuccess(response.user));
    }).catch(error => {
      dispatch(loginFailed());
      throw(error);
    });
  };
}

export function logOutUser() {  
  return function(dispatch) {
    removeLoginSession()
    dispatch(logout());
    return 1
  };
}

export function updateSubscription() {  
  return function(dispatch) {
    return pitchBookApi.getSubscription().then(response => {
      dispatch(updateSubscriptionSuccess(response));
    }).catch(error => {
      throw(error);
    });
  };
}
