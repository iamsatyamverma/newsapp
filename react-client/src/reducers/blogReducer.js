import { blogConstants } from '../constants/action-types.js';  

const initialState = {
  blogs: {}
};

const blogReducer = (state = initialState, action) => {

  switch (action.type) {

    case blogConstants.LOAD_BLOGS_SUCCESS:
    let blogDict = {}
     
        for (let blog of action.blogs){
            blogDict[blog.id] = blog;
        }
      return { ...state, blogs: blogDict};

    default:
      return state;
  }

};

export default blogReducer