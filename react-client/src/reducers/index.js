import { ADD_ARTICLE, LOAD_PITCHBOOKS_SUCCESS } from "../constants/action-types";
import { reducer as formReducer } from 'redux-form';
import pitchBookReducer from './pitchBookReducer.js'
import UserReducer from './userReducer.js'
import { combineReducers } from 'redux';
import blogReducer from "./blogReducer";

const rootReducer = combineReducers({
  form: formReducer,
  pitchbook: pitchBookReducer,
  user:UserReducer,
  blog: blogReducer
});

export default rootReducer;