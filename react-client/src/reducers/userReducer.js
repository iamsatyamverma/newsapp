import { userConstants } from '../constants/action-types.js';  
import { checkIsLoggedIn, createLoginSession } from '../service/utilService.js';
// import { initialState } from './initialState.js';

let user = sessionStorage.getItem('user');
let isLoggedIn = checkIsLoggedIn();

const initialState = {
  loginUser: isLoggedIn ? JSON.parse(user): {},
  profileUser: {},
  isLoggedIn: isLoggedIn,
  loginRequest: false,
  loginError:false
};

const UserReducer = (state = initialState, action) => {
  switch (action.type) {
    
    case userConstants.REGISTER_SUCCESS:
                      
      return { ...state, signupUser: action.user};
    break;
    
    case  userConstants.LOGIN_REQUEST:
    return { ...state, loginUser: {}, isLoggedIn:false, loginRequest: true, loginError: false};
    break;

    case  userConstants.LOGIN_SUCCESS:
      return { ...state, loginUser: action.user, isLoggedIn:true, loginRequest:false, loginError:false};
    break;

    case  userConstants.LOGIN_FAILURE:
    return { ...state, loginUser: action.user, isLoggedIn:false, loginRequest:false, loginError:true};
    break;

    case userConstants.LOGOUT:
      return { ...state, loginUser: {}, isLoggedIn:false, loginRequest:false, loginError:false};
    break;

    case userConstants.PROFILE_REQUEST_SUCCESS:
      let userProfile = {};
      if (action.user.length) {
        userProfile =  action.user[0];
      }
      return { ...state, profileUser: userProfile};
    break;

    case userConstants.PROFILE_UPDATE_SUCCESS:
      let userProfileDetails = action.user
      return { ...state, profileUser: userProfileDetails};
    break;

    case userConstants.SUBSCRIPTION_UPDATE_SUCCESS:
      let response = action.response
    return { ...state, subscription: response};
  break;

    default:
      return state;
  }

};

export default UserReducer