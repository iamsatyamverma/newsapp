import { pitchbookConstants } from '../constants/action-types.js';  
// import { initialState } from './initialState.js';

const initialState = {
  pitchbooks: {'special':[],
              'past':[],
              'current':[]
            }
};

const pitchBookReducer = (state = initialState, action) => {
  let pitchbooks = {...state.pitchbooks};

  switch (action.type) {
    
    case pitchbookConstants.LOAD_PITCHBOOKS_SUCCESS:
      pitchbooks['special'] = [];
      pitchbooks['past'] = [];

      for (let pitchBook of action.pitchBooks){
        if (pitchBook['special'] == '1'){
          pitchbooks['special'].push(pitchBook);
        } else if(pitchBook['special'] == '0') {
          pitchbooks['past'].push(pitchBook);
        }
      }

      return { ...state, pitchbooks: pitchbooks};

    case pitchbookConstants.LOAD_TOP_PITCHBOOKS_SUCCESS:
      pitchbooks['current'] = [];
      
      if (action.pitchBooks.length) {
        pitchbooks['current'] = [action.pitchBooks[0]];
      }

      return { ...state, pitchbooks: pitchbooks};

    default:
      return state;
  }

};

export default pitchBookReducer