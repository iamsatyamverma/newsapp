import _ from 'lodash';

export const getParameterByName = (name, url) => {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

export const postfixLocale = (path, locale) => {
    return `/${_.trim(path, '/')}?locale=${locale}`
}

export const checkIsLoggedIn = res => {
    if (sessionStorage.getItem('jwt'))
        return true;
    else
        return false;
}

export const createLoginSession = (user, token) => {
    sessionStorage.setItem('jwt', token);
    sessionStorage.setItem('user', JSON.stringify(user));
}

export const getAccessToken = () => {
    let USER_TOKEN = sessionStorage.getItem('jwt');
    return 'Token '.concat(USER_TOKEN);
}

export const removeLoginSession = () => {
    sessionStorage.clear()
}

export const getLocale = () => sessionStorage.getItem('locale') || "en";

export const setLocale = (locale) => {
    locale = 'en';
    sessionStorage.setItem('locale', locale);
    window.location.reload();
}

export const getStringOfJson = (json_data) => {
    var stringJson = JSON.stringify(json_data);
    var string_test = stringJson.replace(/[\[\]|[{"}]/g, '');
    if (string_test.length > 250) {
        return "Server Error";
    }
    return string_test;
};