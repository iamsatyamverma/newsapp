import fetch from 'isomorphic-fetch'
import axios from 'axios';
import { getLocale, getAccessToken } from '../service/utilService';

const apiUrl = 'http://api.afarinsights.com/api';
const wpApiUrl = 'http://wpapi.afarinsights.com/wp-json/wp/v2';

class pitchBookApi {
  
    static wpApiGet(relativeUrl, params){

        let url = wpApiUrl + relativeUrl;
        let locale = getLocale();
        params['filter[lang]'] = locale;

        return axios.get(url, {params : params});
    }

    static apiGet(relativeUrl, params){

      const AuthStr = getAccessToken()

      let url = apiUrl + relativeUrl;
      params['lang'] = getLocale();
      return axios.get(url, {params : params, headers: { Authorization: AuthStr }});
    }
    
    static apiPost(relativeUrl, data){
      const AuthStr = getAccessToken()

      let url = apiUrl + relativeUrl;
      return axios.post(url, data, {headers: { Authorization: AuthStr }});
    }   

    static apiPut(relativeUrl, data){
      let url = apiUrl + relativeUrl;
      return axios.put(url, data);
    } 

    static apiDelete(relativeUrl){
      let url = apiUrl + relativeUrl;
      return axios.delete(url);
    }

    static getAllPitchBooks() {
      return this.wpApiGet('/pitchbook', {'order':'asc'}).then(response => {
        return response.data;
      }).catch(error => {
        throw error;
      });
    }

    static getTopPitchBook() {
      return this.wpApiGet('/pitchbook', {'order':'asc', 'filter[tag]':'current-pitchbook'}).then(response => {
        return response.data;
      }).catch(error => {
        return error;
      });
    }

    static getAllBlogs() {
      return this.wpApiGet('/blogs', {'order':'asc'}).then(response => {
        return response.data;
      }).catch(error => {
        throw error;
      });
    }

    static register(user) {
  
      return axios.post(apiUrl+'/auth/register', user)
      .then(response => 
          { 
            return response.data;
          }).catch(error => {
            throw(error.response.data);
          });
    }
    
	static updateProfile(user) {
      return this.apiPost('/profile/', user)
      .then(response => 
          {             
            return response.data;
          }).catch(error => {
            throw(error.response.data);
          });
    }

    static getUserProfile() {
      return this.apiGet('/profile/', {})
      .then(response => 
          { 
            return response.data;
          }).catch(error => {
            throw(error.response.data);
          });
    }

    static getSubscription() {
      return this.apiGet('/paypal/payment_process/', {})
      .then(response => 
          { 
            let redirectLocation = apiUrl + "/paypal/payment_process_redirect/?item_name=" + response.data.item_name + "&amount=" + response.data.amount + "&invoice=" + response.data.invoice + "&currency_code=" + response.data.currency_code;
            window.location = redirectLocation;
            return response.data;
          }).catch(error => {
            window.location = "http://afarinsights.com/#/login";
            throw(error.response.data);
          });
    }
    
    static login(credentials) {
      return axios.post(apiUrl+'/auth/login', credentials)
      .then(response => 
        {
          return response.data;          
        }).catch(error => {
          throw(error.response.data);
        });
  }

}
  
  export default pitchBookApi;  